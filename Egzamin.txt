====================
JAVA
====================

1. Wyskoczy błąd - zmienna text nie ma przypisanej wartości

2. true 
   true

3. IOException

4. .class

5. _helloWorld

6. true
   false
   false
   true
   true
   true

7. wyszkoczy błąd - zmienna a,b,c nie ma przypisanej wartości / zmienne a,b,c nie zostaną zainicjalizowane

8.  Wyskoczy błąd - nie można dzielić przez 0

9. W int: 

2 do 15 daje 32768 -> short jest do 32767 więc musimy użyc inta

short - 2 bajty - zakres od -32 768 do 32 767
int - 4 bajty - zakres od -2 147 483 648 do 2 147 483 647

10. 1
    3
    1
    3

11.25

==============================
SELENIUM:
==============================

1. 

a) Rozwiązanie Selenium Grid składa się z Hub’a, który rozdziela testy oraz Node’ów, które je
wykonują. Testy są kierowane do Hub’a, który posiada listę zarejestrowanych (podpiętych do
niego) Node’ów. To do nich właśnie kieruje komendy, które Node’y mają wykonać.
b) HUB - punkt centralny każdego Grida. Miejsce, do którego są wysyłane nasze testy. Każdy Grid
składa się z dokładnie jednego Huba. Instrukcje (testy) najczęściej trafiają do niego z serwera
Continuous Integration, który zleca ich wykonanie. Mogą też zostać wysłane z komputera osoby
piszącej testy, itp. Hub, po otrzymaniu instrukcji, przekazuje je do Node’ów, którymi zarządza.
NODE - miejsce, gdzie testy są faktycznie wykonywane. To tutaj uruchamiane są
przeglądarki. Każdy node podłączając się do Huba informuje go o swoich możliwościach.
Możliwości te (tzw. capabilities) możemy ustawić inne dla każdego z Node’ów. Może to być np.
maksymalna liczba otwartych okien przeglądarki, informacja jakie przeglądarki mogą zostać
obsłużone, itp. Jest to szczególnie przydatne, gdy maszyny, które wykonują testy mają różną
specyfikację.
c) Tak

2. używając driver.get("adres url")

3. @BeforeEach – metody oznaczone tą adnotacją będą wykonane przed każdym testem.
   @BeforeAll – metody oznaczone tą adnotacją będą wykonane przed wszystkimi innymi metodami w klasie

4. findElement - zwraca jeden element pasujący do kryterium, rzuca wyjątek NoSuchElementException jeżeli kryterium nie jest spełnione
   findElements - zwraca listę elementów pasujących do kryterium, zwraca pustą listę jeżeli kryterium nie jest spełnione

5. a) //*[@id="newPassword1fd8ff0e-35a3-4f7a-8f84-3142efc16b66"]
   b) #\32 8f22a55-0311-4f6c-b3c4-f7b7bc921bcd

6. Tryb headless jest opcją, która pozwala uruchamiać testy Selenium WebDriver bez graficznego interfejsu użytkownika przeglądarki. Ten tryb wspierany jest przez drivera dla Chrome i Firefox.
   Możemy wykonać screenshot okna dla headles browser.

7. Implicity Wait - oczekiwanie bezwarunkowe. (czeka tyle ile mu się kazało)
   Explicit wait - Explicit Wait to oczekiwanie warunkowe (czekamy aż będzie spełniony określony warunek (np. poczekaj aż element jest widoczny/klikalny).
   Fluent Wait - określamy maksymalny czas czekania, oraz interwał z jakim będą podejmowane próby znalezienia elementu.

8. Page Object Model - Jest to sposób pisania testów polegający na tym, że każdą ze stron danej aplikacji webowej przedstawiamy jako tzw. Page object. Każdy z elementów na stronie, którą testujemy możemy przedstawić w naszej implementacji jako obiekt typu IWebElement 
   

========================
WEBSERVICES
========================

1.	SOAP - Simple Object Access Protocol. Zakłada on, że serwis posiada pewien zbiór operacji, które przyjmują określone argumenty. Każdy serwis SOAP powinien udostępniać plik WSDL który opisuje oferowane funkcjonalności, tj. jak nazywa się każda operacja, jakie i jakiego typu dane przyjmuje.
        REST Representational State Transfer. W tym przypadku adresy URL są
	pewnego rodzaju identyfikatorami. Wysyłamy pod te adresy zapytania. Wykonywana przez serwis czynność jest określana nie tylko przez adres URL, ale i metodę HTTP.

	
2. GET, PUT, POST, DELETE
3. POST wysyła zmienne w sposób ukryty, a GET w linku.
4. POST - Dodanie nowych danych
   PUT - Edycja 
5.	Postman, SoupUI, RestAssured dla Javy, może być również Jmeter
6.	Niewielkie informacje wysyłane przez serwis internetowy, który jest odwiedzany, są zapisywane na urządzeniu końcowym, z którego korzystamy podczas przeglądania stron.
7.	Path Params (parametry adresu URL) są używane, gdy chcemy zidentyfikować zasób.	
	Query parameters - to zdefiniowany zestaw parametrów dołączonych na końcu adresu URL. Są to rozszerzenia adresu URL, które służą do definiowania określonej treści lub działań.

========================
SQL
========================

1. select * from classicmodels.offices
2. select count(*) from classicmodels.customers
3. select max(creditLimit) from classicmodels.customers
4. update classicmodels.customers
   set contactLastName ='Franco2'
   where contactLastName ='Franco'
5. select * from classicmodels.offices where country ='USA'
6. select * from classicmodels.customers where contactLastName like '%an'
7. select count(*), country from classicmodels.customers group by country
8. select city, jobTitle from classicmodels.employees
   inner join classicmodels.offices
   on classicmodels.employees.officeCode = classicmodels.offices.officeCode;
