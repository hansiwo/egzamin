import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Zadanie16 {
    public static void main(String[] args) {
        List<Integer> listArray = new ArrayList<>();
        Random random = new Random();
        long startTime = System.currentTimeMillis();


        for (int i = 0; i < 1000000; i++) {
            listArray.add(random.nextInt(100000));
        }
        long endTime = System.currentTimeMillis();
        int array_time = (int) (endTime - startTime);

        List<Integer> listLinked = new LinkedList<>();
        long linkedStartTime = System.currentTimeMillis();

        for (int i = 0; i < 1000000; i++) {
            listLinked.add(random.nextInt(100000));
        }
        long linkedEndTime = System.currentTimeMillis();
        int linked_time = (int) (linkedEndTime - linkedStartTime);

        System.out.println("Czas dodania 100000 liczb całkowitych do ArrayList zajmuje: " + (endTime - startTime) + " ms.");
        System.out.println("Czas dodania 100000 liczb całkowitych do LinkedList zajmuje: " + (linkedEndTime - linkedStartTime) + " ms.");
        System.out.println("Różnica czasów wynosi " +(linked_time - array_time)+ " ms.");
    }
}
