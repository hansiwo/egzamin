public class Zadanie14 {
    public static void main(String[] args) {

        System.out.println(silniaFor(5));
        System.out.println(silniaRekurencja(5));
        System.out.println(silniaWhile(6));

    }

    public static int silniaFor(int n) {
        int iloczyn = 1;
        for (int i = 1; i <= n; i++) {
            iloczyn *= i;
        }
        return iloczyn;
    }

    public static int silniaRekurencja(int n) {
        if (n == 0)
            return 1;
        else
            return (n * silniaRekurencja(n - 1));
    }

    public static int silniaWhile(int n) {
        int iloczyn = 1;
        while (n > 1) {
            iloczyn = iloczyn * n;
            n -= 1;
        }
        return iloczyn;
    }
}
